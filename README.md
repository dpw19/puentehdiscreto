# Puente H Discreto

Puente H discreto optoaislado

Este diseño se basa en los transistores BJT complementarios de mediana potencia [TIP41](https://www.onsemi.com/pdf/datasheet/tip41c-d.pdf) y [TIP42](https://www.onsemi.com/pdf/datasheet/tip42c-d.pdf).

La corriente máxima de estos transistores es de 6A y -6A, sin embargo se recomienda su operación a 3A con buenos disipadores de calor.

El control cuenta con optoaislamiento.

# To do
Colocar los transistores de forma que la disipación térmica sea manejable

# Integración

Este diseño puede combinarse con:

* [SpeedControl](https://gitlab.com/dpw19/speedControl): Código compatible con Arduinos basados en el ATMEGA328P
* [NanoBase](https://gitlab.com/dpw19/nanoBase): Base para el módulo Arduino Nano

El PCB es ideal para control de motores de CD con escobillas.

![01puenteHDiscreto.png](/img/01puenteHDiscreto.png)

![02puenteHDiscreto.png](/img/02puenteHDiscreto.png)

![03puenteHDiscreto.png](/img/03puenteHDiscreto.png)
